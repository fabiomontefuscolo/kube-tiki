<?php
class Tiki
{
	private $pdo;

	private function getPdo()
	{
		if (empty($this->pdo)) {
			if (file_exists('db/local.php')) {
				include('db/local.php');
			} else {
				$dbs_tiki  = getenv('TIKI_DB_NAME') ?: 'tikiwiki';
				$user_tiki = getenv('TIKI_DB_USER') ?: 'tiki';
				$pass_tiki = getenv('TIKI_DB_PASS') ?: 'wiki';
				$host_tiki = getenv('TIKI_DB_HOST') ?: 'db';
			}

			$dsn = $dsn = "mysql:host=${host_tiki};dbname=${dbs_tiki}";
			$this->pdo = new PDO($dsn, $user_tiki, $pass_tiki);
		}
		return $this->pdo;
	}

	public function getPref($prefName, $fresh = true)
	{
		$query = 'SELECT value FROM tiki_preferences WHERE name = ?';
		$key = $query . $prefName;
		$cached = false;
		$ret = apcu_fetch($key, $cached);

		if ($fresh || ! $cached) {
			$pdo = $this->getPdo();
			$stm = $pdo->prepare($query);
			$stm->bindValue(1, $prefName);
			$stm->execute();
			$ret = $stm->rowCount() === 1 ? $stm->fetchColumn() : '';
			apcu_store($key, $ret, 60);
		}

		return $ret;
	}
}

$tiki = new Tiki();

// syncthing will flood this script with request, so cache it
$session_name =	$tiki->getPref('session_cookie_name', false);

if (! empty($session_name)) {
	session_name($session_name);
}
session_start();

$userOk = ! empty($_SESSION)
	&& ! empty($_SESSION['u_info'])
	&& ! empty($_SESSION['u_info']['login'])
	&& ! empty($_SESSION['u_info']['id'])
	&& ! empty($_SESSION['u_info']['group'])
;

if (! $userOk) {
	header('HTTP/1.0 401 Unauthorized');
}
header('Content-Type: text/pain');
